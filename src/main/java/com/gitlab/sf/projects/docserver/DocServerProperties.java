package com.gitlab.sf.projects.docserver;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("docserver")
@Data
public class DocServerProperties {
    /**
     * Title the user will see in the browser
     */
    private String docTitle;
    /**
     * Path where documentations are placed in. The app
     * scans the directory assuming an underlying 3 level layout like "my-group/my-artifact/my-version"
     */
    private String docPath;

    /**
     * @return normalized path with ending '/', and all '//' replaced by single '/'
     */
    public String getDocPath() {
        return docPath.concat("/").replaceFirst("(/){2,}", "/");
    }
}
