package com.gitlab.sf.projects.docserver.impl;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;


/**
 * Implements an {@link ArtifactVersion}
 */
@Getter
@Slf4j
class ArtifactVersion {
    private final File versionRoot;
    private final Artifact artifact;
    private final String version;
    private final String path;

    private static final Pattern RELEASE_VERSION_PATTERN = Pattern.compile("[1-9]{1}[0-9]*\\.[0-9]*\\.[0-9]*");

    public ArtifactVersion(Artifact artifact, ArtifactItemInfo artifactItemInfo) {
        Assert.notNull(artifact);
        Assert.notNull(artifactItemInfo);

        this.versionRoot = artifactItemInfo.getFile().getParentFile();
        this.artifact = artifact;
        this.version = artifactItemInfo.getVersion();
        this.path = artifactItemInfo.getPath();
    }

    public String getGroupId() {
        return artifact.getGroupId();
    }

    public String getArtifactId() {
        return artifact.getArtifactId();
    }

    public static int compareTo(ArtifactVersion a, ArtifactVersion b) {
        String[] versionA = a.getVersion().split("-")[0].split("\\.");
        String[] versionB = b.getVersion().split("-")[0].split("\\.");

        // check parts of the version
        int result = 0;
        for(int i = 0; i < versionA.length; ++i) {
            String ai = versionA[i];
            String bi = versionB[i];
            if(StringUtils.isNumeric(ai) && StringUtils.isNumeric(bi)) {
                Integer va = Integer.parseInt(ai);
                Integer vb = Integer.parseInt(bi);
                result = va.compareTo(vb);
                if(result != 0) {
                    break;
                }
            } else {
                log.warn("One version part of '{}' and '{}' is not numeric", ai, bi);
            }

        }

        // check snapshot
        if(result == 0) {
            result = a.isRelease()?1:-1;
        }

        return result;
    }

    public boolean isRelease() {
        return RELEASE_VERSION_PATTERN.matcher(getVersion()).matches();
    }

    @Override
    public String toString() {
        return Artifact.print(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArtifactVersion)) return false;

        ArtifactVersion that = (ArtifactVersion) o;

        if (!artifact.equals(that.artifact)) return false;
        return version.equals(that.version);

    }

    @Override
    public int hashCode() {
        int result = artifact.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    public Group getGroup() {
        return artifact.getGroup();
    }

    /**
     * TODO: expensive on heavy load (many file operations) - implement some caching
     *
     * @return first pdf file if found any
     */
    public File getPdfFile() {
        File[] matchingFiles = versionRoot.listFiles((FilenameFilter) new SuffixFileFilter("pdf"));
        return matchingFiles.length == 0
                ? null
                : matchingFiles[0];
    }
}
